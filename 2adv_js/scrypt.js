const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  const div = document.querySelector('#root')

  books.map(({author, name, price}) =>{
    const ul = document.createElement('ul')

    try{
        if (author === undefined || name === undefined || price === undefined) {
            throw new Error('Error');
          }

        const li = document.createElement('li')
        ul.append(li)
        div.append(ul)
       
    }
    catch (error) {
    console.log(error);
  }
  
  ul.append(`${author}: `, `${name} - `, price)

  })