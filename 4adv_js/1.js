fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {

    films.forEach(film => {
      console.log(`№${film.episodeId}: ${film.name}`);
      console.log(`Краткое содержание: ${film.openingCrawl}`);

      
      Promise.all(film.characters.map(characterUrl =>
        fetch(characterUrl).then(response => response.json())
      ))
        .then(characters => {
          console.log(`Персонажи епизода ${film.episodeId}:`);
          characters.forEach(character => {
            console.log(character.name);
          });
        });
    });
  });