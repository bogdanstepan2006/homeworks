// links
const links = document.querySelectorAll('a')
links.forEach((elem)=>{
    elem.addEventListener('click', function(event){
        event.preventDefault()
    })
})


// services selector
const servicesLi = document.querySelectorAll('.services-list li');
const liDiscription = document.querySelectorAll('.selector-image');

servicesLi.forEach((elem) => {
  elem.addEventListener('click', (event) => {

    liDiscription.forEach((content) => {
      content.classList.add('hide');
    });

    const liContent = document.querySelector(`div[data-tab="${event.target.dataset.tab}"]`);
    console.log(liContent)
 
    liContent.classList.remove('hide');

    servicesLi.forEach((elem) => {
      elem.classList.remove('active');
    });

    event.target.classList.add('active');
  });
});

//image-net button
const button = document.querySelector('.section-3-button-wrap');
const hiddenDiv = document.querySelector('.hidden-images')
console.log(hiddenDiv)


 

hiddenDiv.style.display = 'none'
button.addEventListener('click', evt => {
    evt.preventDefault();
    button.style.display = 'none';
    hiddenDiv.style.display = 'block'
  })


// image-net
const imagesLi = document.querySelectorAll('.images-net-services');
const imagesContent = document.querySelectorAll('.image-net-li-images');

imagesLi.forEach(tab => {
  tab.addEventListener('click', function(event){
    imagesLi.forEach(elem => {
      elem.classList.remove('glow');
    });
    event.target.classList.add('glow');

  
  const selectedDataSet = event.target.getAttribute('data-set'); 
  imagesContent.forEach(content => {
    const contentDataSet = content.getAttribute('data-set');
    if (contentDataSet === selectedDataSet || selectedDataSet === 'all') {
      content.style.display = 'block';  
    } else {
      content.style.display = 'none';
    }
   if(selectedDataSet === 'all'){
    hiddenDiv.style.display = 'block'
   }
   if(selectedDataSet !== 'all'){
    hiddenDiv.style.display = 'flex'
   }

  });
  });
});
