let name = prompt("Enter your name");
let age = prompt("Enter your age");

// перевірка коректності введених даних
while (!name || isNaN(age)) {
  alert("Please enter the number!");
  age = prompt("Enter your age");
}

// перевірка віку
if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  const confirmed = confirm("Are you sure you want to continue?");
  if (confirmed) {
    alert(`Welcome, ${name}!`);
  } else {
    alert("You don`t want to confirm");
  }
} else {
  alert(`You are older than 22`);
}



