class Employee {
  constructor(name, age, salary) {
  this._name = name;
  this._age = age;
  this._salary = salary;
  }
  
    get name() {
      return this._name;
  }
  
  set name(name) {
      this._name = name;
  }
  
  get age() {
      return this._age;
  }
  
  set age(age) {
      this._age = age;
  }
  
  get salary() {
      return this._salary;
  }
  
  set salary(salary) {
      this._salary = salary;
  }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
  }
  }
  
const programmer1 = new Programmer("John", 30, 5000, "Python");
const programmer2 = new Programmer("Dave", 24, 6000, "C++");



//1
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Language:", programmer1.lang);
//2
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Language:", programmer2.lang);