let arr = ["hello", "world", 23, 49, null];

let result = filterBy(arr, "string");

function filterBy(arr, type) {
    return arr.filter((item) => typeof item !== type);
  }


console.log(result); 