function createNewUser(){
    let firstName = prompt("Enter your firstname:")
    let lastName = prompt("Enter your lastname:")

    while(!isNaN(firstName) || !isNaN(lastName)){
        alert("Enter your real name!")
        firstName = prompt("Enter your firstname:")
        lastName = prompt("Enter your lastname:")
    }

    let newUser = {
        firstName,
        lastName,
        getLogin(){
            return `${newUser.firstName[0].toLowerCase()}${newUser.lastName.toLowerCase()}`
        }
    }
    return newUser;
}

console.log(createNewUser().getLogin())