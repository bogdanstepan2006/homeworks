import React, { Component } from 'react';
import Modal from './components/Modal';
import Button from './components/Button';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModal: false,
        };
    }

    handleModal = () => {
        this.setState(prevState => ({
            isModal: !prevState.isModal,
        }));
    };

    handleOutside = event => {
        if (!event.target.closest('.modal')) {
            this.handleModal();
        }
    };

    render() {
        const { isModal } = this.state;

        return (
            <>
                <Button 
                showFirstModal={this.handleModal}
                showSecondModal={this.handleModal}
                text={'Modal'}
                backgroundColor ='grey'
                 />
                {isModal && (
                    <Modal
                        header={'Do you want to delete?'}
                        text={"Once you delete this file, it won’t be possible to undo this action. Are you sure, you want to delete it?"}
                        handleOutside={this.handleOutside}
                        closeModal={this.handleModal}
                    />
                )}
            </>
        );
    }
}

export default App;
