import React from 'react'


class Button extends React.Component{
    render(){
        const {showFirstModal, showSecondModal, backgroundColor, text } = this.props
        return(
            <div>
            <button onClick={showFirstModal} style={{ backgroundColor: '#859989' }}>Open first {text}</button>
            <br />
            <br />
            <button onClick={showSecondModal} style={{ backgroundColor: backgroundColor }}>Opne second {text}</button>
            </div>
        )
    }
}
export default Button