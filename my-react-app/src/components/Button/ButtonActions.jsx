import React from "react";

class ButtonActions extends React.Component{

   
    render(){
        const {closeModal} = this.props
        return(
            <div className="button-wrapper">
            <button  className="btn" type="button" onClick={closeModal}>OK</button>
            <button onClick={closeModal} className="btn" type="button">Cancel</button>
        </div>
        )
    }
}
export default ButtonActions;